import axios from 'axios'

export const getCustomerProfile = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/customer/profile`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const setCustomerProfile = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/customer/profile`, data, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const getCartItems = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/customer/carts`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const addCartItem = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/customer/add-cart?itemAmount=${data.itemAmount}&itemId=${data.itemId}`, data, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const deleteCartItem = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/customer/delete-cart?cartId=${data.cartId}`, data, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const buyFromCart = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/customer/buy-from-cart`, data, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const getTransactionHistory = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/customer/transaction-history`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e))
}