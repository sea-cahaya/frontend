import axios from 'axios';

export const addItem = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/merchant/add-item`, data.body, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const getItemById = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/merchant/items/${data.id}`, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const editItem = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/merchant/item/${data.id}`, data.body, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const deleteItemById = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/merchant/item/delete/${data.id}`, {}, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const getItems = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/merchant/items`, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const getItemsByMerchantId = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/merchant/items/${data.id}`, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const getMerchantProfile = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/merchant/profile`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const setMerchantProfile = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/merchant/profile`, data, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const getRequestItem = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/merchant/request-item`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const getWalletBalance = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/merchant/wallet/balance`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const getWalletHistory = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/merchant/wallet/history`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}

export const transferWalletBalance = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/merchant/wallet/transfer`, data, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}