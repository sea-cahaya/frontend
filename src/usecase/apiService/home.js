import axios from 'axios';

export const getAllItems = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/home/items`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};
