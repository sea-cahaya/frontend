import axios from 'axios';

export const getMerchantApplication = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/admin/merchant`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const confirmMerchantApplication = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/admin/confirm-merchant?username=${data.username}`, {}, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const getTransactions = (data, callback) => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/admin/transactions`, { headers: { Authorization: data } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const acceptTransaction = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/admin/accept-transaction?transactionId=${data.transactionId}`, {}, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const rejectTransaction = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/admin/reject-transaction?transactionId=${data.transactionId}`, {}, { headers: { Authorization: data.authorization } })
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};