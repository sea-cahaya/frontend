import axios from 'axios';

export const login = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/auth/signin`, data)
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const loginAdmin = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/auth/signin`, data)
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const registerCustomer = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/auth/customer-signup`, data)
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const registerMerchant = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/auth/merchant-signup`, data)
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
};

export const registerAdmin = (data, callback) => {
    axios.post(`${process.env.REACT_APP_API_URL}/api/auth/admin-signup`, data)
        .then(res => callback.onSuccess(res))
        .catch(e => callback.onError(e));
}