import React, { Fragment, useEffect } from "react";
import "./template.css";
import { Link } from "react-router-dom";
import { getLocalStorageUserData } from "../usecase/utilities";

/**
 * Default template.
 */
export default (props) => {
  const PROFILE = "Profile";
  const ROLE_MERCHANT = "ROLE_MERCHANT";
  let name = getLocalStorageUserData("username") || PROFILE;
  let isCustomer = getLocalStorageUserData("roles") === "ROLE_CUSTOMER";

  useEffect(() => {
    const cssLink = document.getElementById("cssLink");
    const themeToggler = document.getElementById("themeToggler");
    const mainNav = document.getElementById("main-nav");
    if (!localStorage.getItem("mode")) {
      if (
        window.matchMedia &&
        window.matchMedia("(prefers-color-scheme: dark)").matches
      ) {
        // dark mode
        localStorage.setItem("mode", "dark");
        mainNav.setAttribute(
          "class",
          "navbar navbar-dark navbar-expand-md fixed-top np-bg-background"
        );
        cssLink.setAttribute("href", "./css/dark/neumorph-full-dark.min.css");
      } else {
        localStorage.setItem("mode", "light");
        mainNav.setAttribute(
          "class",
          "navbar navbar-light navbar-expand-md fixed-top np-bg-background"
        );
        cssLink.setAttribute("href", "./css/light/neumorph-full.min.css");
      }
    }
    themeToggler.addEventListener("click", () => {
      if (cssLink.href.includes("dark")) {
        localStorage.setItem("mode", "light");
        mainNav.setAttribute(
          "class",
          "navbar navbar-light navbar-expand-md fixed-top np-bg-background"
        );
        cssLink.setAttribute("href", "./css/light/neumorph-full.min.css");
      } else {
        localStorage.setItem("mode", "dark");
        mainNav.setAttribute(
          "class",
          "navbar navbar-dark navbar-expand-md fixed-top np-bg-background"
        );
        cssLink.setAttribute("href", "./css/dark/neumorph-full-dark.min.css");
      }
    });
    if (localStorage.getItem("mode") === "light") {
      console.log("dark", cssLink);
      mainNav.setAttribute(
        "class",
        "navbar navbar-light navbar-expand-md fixed-top np-bg-background"
      );
      cssLink.setAttribute("href", "./css/light/neumorph-full.min.min.css");
    } else if (localStorage.getItem("mode") === "dark") {
      console.log("light", cssLink);

      mainNav.setAttribute(
        "class",
        "navbar navbar-dark navbar-expand-md fixed-top np-bg-background"
      );
      cssLink.setAttribute("href", "./css/dark/neumorph-full-dark.min.css");
    }
  }, []);
  return (
    <div className="nav-padding">
      <nav
        id="main-nav"
        className={
          "navbar navbar-dark navbar-expand-md fixed-top np-bg-background"
        }
      >
        <div className="container-fluid">
          <Link
            to={
              getLocalStorageUserData("roles") === ROLE_MERCHANT
                ? "/profile"
                : "/"
            }
            style={{
              textDecoration: `none`,
              padding: "4px 16px",
            }}
            className="nav-link"
          >
            Cahaya
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarCollapse"
            aria-controls="navbarCollapse"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav mr-auto mb-2 mb-md-0">
              {name === PROFILE ? (
                <li className="nav-item">
                  <Link
                    to="/login"
                    style={{
                      textDecoration: `none`,
                    }}
                    className="nav-link"
                  >
                    Login
                  </Link>
                </li>
              ) : (
                  ""
                )}
              {name !== PROFILE ? (
                isCustomer ? (
                  <Fragment>
                    <li className="nav-item">
                      <Link
                        to="/cart"
                        style={{
                          textDecoration: `none`,
                        }}
                        className="nav-link"
                      >
                        Cart
                      </Link>
                    </li>
                    <li className="nav-item">
                      <Link
                        to="/transactions-history"
                        style={{
                          textDecoration: `none`,
                        }}
                        className="nav-link"
                      >
                        Transactions History
                      </Link>
                    </li>
                  </Fragment>
                ) : (
                    <li className="nav-item dropdown">
                      <div
                        className="nav-link dropdown-toggle"
                        id="navbarDropdown"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        {name}
                      </div>
                      <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                        <Link
                          to="/profile"
                          style={{
                            textDecoration: `none`,
                          }}
                          className="nav-link"
                        >
                          My Profile
                      </Link>
                        <Link
                          to="/store"
                          style={{
                            textDecoration: `none`,
                          }}
                          className="nav-link"
                        >
                          My Store
                      </Link>
                        <Link
                          to="/request-item"
                          style={{
                            textDecoration: `none`,
                          }}
                          className="nav-link"
                        >
                          My Item Requests
                      </Link>
                        <Link
                          to="/wallet"
                          style={{
                            textDecoration: `none`,
                          }}
                          className="nav-link"
                        >
                          My Wallet
                      </Link>
                      </div>
                    </li>
                  )
              ) : (
                  ""
                )}
              {name === PROFILE ? (
                ""
              ) : (
                  <li className="nav-item">
                    <Link
                      onClick={() => {
                        localStorage.removeItem("u");
                      }}
                      to="/"
                      style={{
                        textDecoration: `none`,
                      }}
                      className="nav-link"
                    >
                      Log Out
                  </Link>
                  </li>
                )}
            </ul>
            <div id="themeToggler">
              <svg
                width="1em"
                height="1em"
                viewBox="0 0 16 16"
                className="bi bi-circle-half"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M8 15V1a7 7 0 1 1 0 14zm0 1A8 8 0 1 1 8 0a8 8 0 0 1 0 16z"
                />
              </svg>
            </div>
          </div>
        </div>
      </nav>
      <main className="container">{props.children}</main>
    </div>
  );
};
