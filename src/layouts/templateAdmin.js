import React, { Fragment } from "react";
import './templateAdmin.css';
import { Link } from "react-router-dom";
import { getLocalStorageUserData } from "../usecase/utilities";

/**
 * Default template.
 */
export default (props) => {
  const APP_NAME = "Cahaya Admin";
  return <div className="nav-padding">
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <div className="container-fluid">
        <div className="navbar-brand">
        {APP_NAME}
        </div>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarCollapse">
          <ul className="navbar-nav mr-auto mb-2 mb-md-0">
            {getLocalStorageUserData('username') ? <Fragment>
              <Link
                to="/admin/merchant"
                style={{
                  textDecoration: `none`,
                }}
                className="nav-link"
              >
                Merchant
              </Link>
              <Link
                to="/admin/transactions"
                style={{
                  textDecoration: `none`,
                }}
                className="nav-link"
              >
                Transactions
              </Link>
            </Fragment> : ''
            }
            {getLocalStorageUserData('username') ? <Link
              onClick={() => { localStorage.removeItem('u'); }}
              to="/admin/home"
              style={{
                textDecoration: `none`,
              }}
              className="nav-link"
            >
              Log Out
              </Link> : <Link
                to="/admin/login"
                style={{
                  textDecoration: `none`,
                }}
                className="nav-link"
              >
                Log In
              </Link>}

          </ul>
        </div>
      </div>
    </nav>
    <main className="container">{props.children}</main>
  </div>
};
