import React, { useState, useEffect } from 'react';
import { getWalletBalance, getWalletHistory, transferWalletBalance } from '../../usecase/apiService/merchant';
import { Template } from '../../layouts';
import './wallet.css';
import { getLocalStorageUserData, convertToRupiah } from '../../usecase/utilities';
import { AlertSuccess } from '../../components/alerts';

export default (props) => {
    const [balance, setBalance] = useState('0');
    const [walletTransactionsHistory, setWalletTransactionsHistory] = useState([]);
    const [transferAmount, setTransferAmount] = useState('');
    const [bankName, setBankName] = useState('');
    const [bankAccountNumber, setBankAccountNumber] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        getWalletBalance(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setBalance(res.data);
                console.log(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    });

    useEffect(() => {
        getWalletHistory(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setWalletTransactionsHistory(res.data);
                console.log(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    });

   const TransferBalance = () => {
       setIsLoading(true);
        transferWalletBalance({ bankName, bankAccountNumber, balance: transferAmount, authorization: getLocalStorageUserData('accessToken') }, {
            onSuccess: (res) => {
                setSuccessMessage(res.data.message);
                setShowSuccessMessage(true);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
        setIsLoading(false);
    }

    const cardStyle = {
        width: "60%",
    };

    const isDisabled = () => {
        return !(bankAccountNumber && bankName && !isLoading)
    };

    return <Template>
        <h2 className="text-center">My Wallet</h2>
        <div id="wallet" style={cardStyle} className="card np-element mx-auto my-3 w-50">
            <div className="card-body">
                <h3 className="h5">Balance</h3>
                <h4 className="card-subtitle text-center mb-2 h2">{convertToRupiah(balance)}</h4>
            </div>
        </div>
        <div>
            <h3 className="h5">Transfer to My Bank Account</h3>
            <form className="form-profile mt-3">
                <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
                <div className="form-label-group">
                    <input
                        onChange={e => {
                            setBankName(e.target.value);
                        }}
                        type="text"
                        id="inputAccountName"
                        className="form-control"
                        placeholder="Account Name"
                        required
                    />
                    <label htmlFor="inputAccountName">Your Bank Account Name</label>
                </div>
                <div className="form-label-group">
                    <input
                        onChange={e => {
                            setBankAccountNumber(e.target.value);
                        }}
                        type="text"
                        id="inputBankAccountNumber"
                        className="form-control"
                        placeholder="Account Number"
                        required
                    />
                    <label htmlFor="inputBankAccountNumber">Your Bank Account Number</label>
                </div>
                <div className="form-label-group">
                    <input
                        onChange={e => {
                            setTransferAmount(e.target.value);
                        }}
                        type="text"
                        id="inputTransferAmount"
                        className="form-control"
                        placeholder="Account Number"
                        required
                    />
                    <label htmlFor="inputTransferAmount">Amount</label>
                </div>
                <button onClick={ TransferBalance } disabled={isDisabled()} className="btn btn-primary mb-3" type="button" >Submit</button>
        </form>
        </div>
        <div className="wallet-transaction-history my-3">
            <h3 className="h5">Wallet Transaction History</h3>
            <WalletTransactionsHistory data={walletTransactionsHistory} />
        </div>
    </Template>
}

const WalletTransactionsHistory = ({data}) => data.map((transaction) => {
    return <div key={transaction.id} className={transaction.nominalIn !== 0 ? "container border border-primary py-2 my-2" : "container border border-danger py-2 my-2"}>
        <div className="row">
            <div className="col">
                {new Date(transaction.time).toLocaleTimeString()}, {new Date(transaction.time).toLocaleDateString()}
            </div>
            <div className="col">
                {transaction.nominalIn !== 0 ? "+" + convertToRupiah(transaction.nominalIn) : "-" + convertToRupiah(transaction.nominalOut)}
            </div>
            <div className="col">
                {transaction.nominalIn !== 0 ? "Transaction from " + transaction.bankName : "Bank Transfer to " + transaction.bankName}
            </div>
        </div>
    </div>
})