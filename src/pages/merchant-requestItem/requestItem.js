import React, { useEffect, useState } from "react";
import { Template } from "../../layouts";
import './requestItem.css';
import { getLocalStorageUserData } from "../../usecase/utilities";
import { getRequestItem } from "../../usecase/apiService/merchant";
import { MerchantRequestItemTable } from "../../components/merchantRequestItemTable";

export default (props) => {
    const [requestItem, setRequestItem] = useState([]);

    useEffect(() => {
        getRequestItem(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setRequestItem(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }, [])
    return <Template>
        <h1>Merchant Request Item</h1>
        <MerchantRequestItemTable data={requestItem} />
    </Template>
}