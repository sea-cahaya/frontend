import React, { useState } from "react";
import { Link } from 'react-router-dom';
import { Template } from "../../layouts";
import './register.css';
import { registerCustomer, registerMerchant } from "../../usecase/apiService/auth";
import { AlertSuccess } from "../../components/alerts";
import { checkPassword, validateEmail } from "../../usecase/utilities";

export default (props) => {
    const [role, setRole] = useState('');
    const [username, setUsername] = useState('');
    const [merchantName, setMerchantName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const [isEmailValid, setIsEmailValid] = useState(true);
    const [isPasswordValid, setIsPasswordValid] = useState(true);
    const [isPasswordMatch, setIsPasswordMatch] = useState(true);

    // between 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character
    const register = () => {
        console.log(validateEmail(email))
        setIsEmailValid(validateEmail(email))
        setIsPasswordMatch(password === confirmPassword)
        setIsPasswordValid(checkPassword(password))
        if(validateEmail(email) && password === confirmPassword && checkPassword(password)) {
            if (role === "customer") {
                registerCustomer({ username, email, password }, {
                    onSuccess: (res) => {
                        setSuccessMessage(res.data.message);
                        setShowSuccessMessage(true);
                        setTimeout(function () {
                            window.location = '/login';
                        }, 3000);
                    },
                    onError: (e) => {
                        console.log(e)
                        alert(e.message);
                    }
                })
            } else if (role === "merchant") {
                registerMerchant({ username, merchantName, email, password }, {
                    onSuccess: (res) => {
                        setSuccessMessage(res.data.message);
                        setShowSuccessMessage(true);
                        setTimeout(function () {
                            window.location = '/login';
                        }, 3000);
                    },
                    onError: (e) => {
                        console.log(e)
                        alert(e.message);
                    }
                })
            } else {
                alert("role not valid.")
            }
        }
    };
    

    const isDisabled = () => {
        return !(role && username && email && password && confirmPassword && password === confirmPassword)
    }

    return <Template>
        <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
        <form className="form-signin">
            <div className="text-center mb-4">
                <h1 className="h3 mb-3 font-weight-normal">Register</h1>
            </div>
            <div className="form-radio">
                <div>
                    I would like to register as a
                </div>
                <div className="form-check">
                    <input onClick={(e) => setRole(e.target.value)} className="form-check-input" value="customer" type="radio" name="flexRadioDefault" id="radioCustomer" />
                    <label className="form-check-label" htmlFor="radioCustomer">
                        Customer
                    </label>
                </div>
                <div className="form-check">
                    <input onClick={(e) => setRole(e.target.value)} className="form-check-input" value="merchant" type="radio" name="flexRadioDefault" id="radioMerchant" />
                    <label className="form-check-label" htmlFor="radioMerchant">
                        Merchant
                    </label>
                </div>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setUsername(e.target.value);
                    }}
                    type="text"
                    id="inputUsername"
                    className="form-control"
                    placeholder="Username"
                    required
                />
                <label htmlFor="inputUsername">Username</label>
            </div>
            {role === "merchant" ? <div className="form-label-group">
                <input
                    onChange={e => {
                        setMerchantName(e.target.value);
                    }}
                    type="text"
                    id="inputMerchantName"
                    className="form-control"
                    placeholder="Merchant Name"
                    required
                />
                <label htmlFor="inputMerchantName">Merchant Name</label>
            </div> : ''}
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setEmail(e.target.value);
                    }}
                    type="email"
                    id="inputEmail"
                    className="form-control"
                    placeholder="Email address"
                    required
                />
                <label htmlFor="inputEmail">Email address</label>
                <div className="invalid-feedback" style={{display: isEmailValid ? '' : 'block'}}>
                    This email format is not valid.
                </div>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setPassword(e.target.value);
                    }}
                    type="password"
                    id="inputPassword"
                    className="form-control"
                    placeholder="Password"
                    required
                />
                <label htmlFor="inputPassword">Password</label>
                <div className="invalid-feedback" style={{display: isPasswordValid ? '' : 'block'}}>
                    Password must be between 8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character.
                </div>
                <div className="invalid-feedback" style={{display: isPasswordMatch ? '' : 'block'}}>
                    Password does not match. 
                </div>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setConfirmPassword(e.target.value);
                    }}
                    type="password"
                    id="inputComfirmPassword"
                    className="form-control"
                    placeholder="Confirm Password"
                    required
                />
                <label htmlFor="inputComfirmPassword">Confirm Password</label>
                <div className="invalid-feedback" style={{display: isPasswordMatch ? '' : 'block'}}>
                    Password does not match.
                </div>
            </div>
            <p>
                Have an account?{" "}
                <Link
                    to="/login"
                    style={{
                        textDecoration: `none`,
                    }}
                >
                    Login
              </Link>
            </p>
            <button onClick={register} className="btn btn-lg btn-primary btn-block" type="button" disabled={isDisabled()}>Create Account</button>
        </form>
    </Template>
}