import React, { useEffect, useState } from "react";
import { Template } from "../../layouts";
import './merchantStore.css';
import { getLocalStorageUserData } from "../../usecase/utilities";
import { ItemCard } from "../../components/itemCard";
import { getItemById } from "../../usecase/apiService/merchant";

export default (props) => {
    const [items, setItems] = useState([]);

    const getItems = () => {
        getItemById({id: props.match.params.id, authorization: getLocalStorageUserData('accessToken')}, {
            onSuccess: (res) => {
                setItems(res.data);
                console.log(res.data)
            },
            onError: (e) => {
                alert(e.message);
            }
        });
    }
    useEffect(getItems, []);
    

    return <Template>
    <h1>Merchant store</h1>
        <div className="row row-cols-1 row-cols-sm-1 row-cols-md-3 g-4">
  
        {items.map((item, index)=>{
            return <div className="col"><ItemCard isPreview={false} data={item} /></div>
        })}
        </div>
    </Template>
};
