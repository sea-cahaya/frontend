import React, { useEffect, useState } from "react";
import { Template } from "../../layouts";
import './store.css';
import { getItems, addItem } from "../../usecase/apiService/merchant";
import { getLocalStorageUserData } from "../../usecase/utilities";
import { EditProfileItemCard, AddProfileItemCard } from "../../components/itemCard";

export default (props) => {
    const [items, setItems] = useState([]);

    const getAllItems = () => {
        getItems({ authorization: getLocalStorageUserData('accessToken') }, {
            onSuccess: (res) => {
                setItems(res.data);
                console.log(res.data)
            },
            onError: (e) => {
                alert(e.message);
            }
        });
    }
    useEffect(getAllItems, []);
    

    const add = (body) => {
        addItem({ body, authorization: getLocalStorageUserData('accessToken') }, {
            onSuccess: (res) => {
                alert(res.data.message)
                console.log(res);
                getAllItems()
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    };

    return <Template>
        <h1>My Store</h1>
        <AddProfileItemCard onAddItem={(data)=>add(data)} />
        <EditProfileItemCards onDeleteItem={()=>{
            alert("item deleted");
            window.location.reload()
        }} items={items} />
    </Template>
};

const EditProfileItemCards = ({ items, onDeleteItem }) => {
    return items.map((item, index) => <EditProfileItemCard data={item} onDeleteItem={onDeleteItem} />)
}