import React, { useState } from "react";
import { Link } from 'react-router-dom';
import { TemplateAdmin } from "../../layouts";
import './admin-login.css';
import { AlertSuccess } from "../../components/alerts";
import { loginAdmin } from "../../usecase/apiService/auth";

export default (props) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');

    const signin = () => {
        loginAdmin({ username, password }, {
            onSuccess: (res) => {
                let message = res.data.message || "Login Success!";
                setSuccessMessage(message);
                setShowSuccessMessage(true);
                localStorage.setItem('u', JSON.stringify(res.data));
                
                setTimeout(function () {
                    window.location = '/admin/merchant';
                }, 3000);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    };

    const isDisabled = () => {
        return !(username && password)
    }

    return <TemplateAdmin>
        <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
        <form className="form-signin">
            <div className="text-center mb-4">
                <h1 className="h3 mb-3 font-weight-normal">Login</h1>
            </div>
            <div className="form-label-group">
                <input 
                    onChange={e=>{
                        setUsername(e.target.value);
                    }} 
                    type="text"
                    id="inputUsername"
                    className="form-control"
                    placeholder="Username"
                    required
                    autoFocus 
                />
                <label htmlFor="inputUsername">Username</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e=>{
                        setPassword(e.target.value);
                    }} 
                    type="password"
                    id="inputPassword"
                    className="form-control"
                    placeholder="Password"
                    required 
                />
                <label htmlFor="inputPassword">Password</label>
            </div>
            <p>
                Don't have an account?{" "}
                <Link
                    to="/admin/register"
                    style={{
                        textDecoration: `none`,
                    }}
                >
                    Create Account
              </Link>
            </p>
            <button onClick={signin} className="btn btn-lg btn-primary btn-block" type="button" disabled={isDisabled()}>Login</button>
        </form>
    </TemplateAdmin>
}