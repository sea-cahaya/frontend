import React from "react";
import { Template } from "../../layouts";
import './profile.css';
import { FormMerchantProfile } from "../../components/formMerchantProfile";
import { getLocalStorageUserData } from "../../usecase/utilities";
import { CustomerProfile } from "..";

export default (props) => {
    if( getLocalStorageUserData("roles") === "ROLE_MERCHANT") {
        return <Template><FormMerchantProfile /></Template>
    } else return <CustomerProfile />
};
