import React, { useEffect, useState } from "react";
import { TemplateAdmin } from "../../layouts";
import './applyMerchant.css';
import { getMerchantApplication } from "../../usecase/apiService/admin";
import { MerchantApplicationTable } from "../../components/merchantApplicationTable";
import { getLocalStorageUserData } from "../../usecase/utilities";

export default (props) => {
    const [applications, setApplications] = useState([]);
    useEffect(() => {
        getMerchantApplication(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setApplications(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }, [])
    return <TemplateAdmin>
        <h1>Merchant Applications</h1>
        <MerchantApplicationTable data={applications} />
    </TemplateAdmin>
}