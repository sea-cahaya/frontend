import React, { useState, useEffect } from 'react';
import Template from '../../layouts/template';
import './checkout.css';
import { getLocalStorageUserData, convertToRupiah } from '../../usecase/utilities';
import { getCustomerProfile, getCartItems } from '../../usecase/apiService/customer';
import { FormPayment } from '../../components/formPayment';

export default (props) => {
    const [profile, setProfile] = useState([]);
    const [items, setItems] = useState([]);
    const [confirmationPayment, setConfirmationPayment] = useState(false);

    useEffect(() => {
        getCustomerProfile(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setProfile(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }, [])

    useEffect(() => {
        getCartItems(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setItems(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        });
    })

    const isDisabled = () => {
        return !(items.length)
    };

    return <Template>
        <h1>Checkout</h1>
        <h2 className="h5" >Shipping Address</h2>
        <div className="card np-element np-shadow-inverse">
            <div className="card-body">
                <h5 className="card-title">{profile.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{profile.phone}</h6>
                <p className="card-text">{profile.address}</p>
            </div>
        </div>
        <hr />
        <table id="table-checkout" className="np-table np-0-8x my-3">
            <thead className="np-table-heading">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Item Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Amount</th>
                </tr>
            </thead>
            <tbody>
                <TableRow data={items} />
                <Total data={items} />
            </tbody>
        </table>
        <span>Transfer your payment to BCA 0002XXXXX01 (Cahaya) / Mandiri 0001XXXXX01 (Cahaya)</span>
        <br />
        <button className="btn btn-primary mt-2 mb-3" disabled={isDisabled()} onClick={ () => setConfirmationPayment(!confirmationPayment) }>Payment Confirmation</button>
    
        {confirmationPayment ? <FormPayment /> : ""}
    </Template>
}

const TableRow = ({ data }) => data.map((item, index) => {
    return <tr key={item.id} className="np-table-row">
        <th className="fit" scope="row">{index + 1}</th>
        <td>{item.itemName}</td>
        <td>{convertToRupiah(item.price)}</td>
        <td>{item.amount}</td>
    </tr>
})

const Total = ({ data }) => {
    const totalItems = data.reduce((amount, item) => (amount += item.amount), 0)
    const subTotal = data.reduce((total, item) => (total += item.price), 0)
    return <tr>
        <td colSpan="4" className="np-table-row">
            <span className="font-weight-bold">SubTotal</span>({totalItems} items) :&nbsp; {convertToRupiah(subTotal)}
        </td>
    </tr>
}