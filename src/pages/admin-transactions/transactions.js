import React, { useEffect, useState } from "react";
import { TemplateAdmin } from "../../layouts";
import './transactions.css';
import { getTransactions } from "../../usecase/apiService/admin";
import { getLocalStorageUserData } from "../../usecase/utilities";
import { MerchantTransactionTable } from "../../components/merchantTransactionsTable";

export default (props) => {
    const [transactions, setTransactions] = useState([]);

    useEffect(() => {
        getTransactions(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setTransactions(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }, [])
    return <TemplateAdmin>
        <h1>Merchant Transactions</h1>
        <MerchantTransactionTable data={transactions} />
    </TemplateAdmin>
}