import React, { useState } from "react";
import { Link } from 'react-router-dom';
import { Template } from "../../layouts";
import './login.css';
import { AlertSuccess } from "../../components/alerts";
import { login } from "../../usecase/apiService/auth";

export default (props) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');

    const signin = () => {
        login({ username, password }, {
            onSuccess: (res) => {
                if (res.data.roles === "ROLE_ADMIN") {
                    alert("You are an admin, you will be redirected to admin login page");
                    window.location = '/admin/login';
                } else {
                    let message = res.data.message || "Login Success!";
                    setSuccessMessage(message);
                    setShowSuccessMessage(true);
                    localStorage.setItem('u', JSON.stringify(res.data));

                    setTimeout(function () {
                        window.location = res.data.roles === "ROLE_CUSTOMER" ? '/' : '/profile';
                    }, 3000);
                }
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    };

    const isDisabled = () => {
        return !(username && password)
    }

    return <Template>
        <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
        <form className="form-signin">
            <div className="text-center mb-4">
                <h1 className="h3 mb-3 font-weight-normal">Login</h1>
            </div>
            <div className="form-label-group">
                <input 
                    onChange={e=>{
                        setUsername(e.target.value);
                    }} 
                    type="text"
                    id="inputUsername"
                    className="form-control"
                    placeholder="Username"
                    required
                    autoFocus 
                />
                <label htmlFor="inputUsername">Username</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e=>{
                        setPassword(e.target.value);
                    }} 
                    type="password"
                    id="inputPassword"
                    className="form-control"
                    placeholder="Password"
                    required 
                />
                <label htmlFor="inputPassword">Password</label>
            </div>
            <p>
                Don't have an account?{" "}
                <Link
                    to="/register"
                    style={{
                        textDecoration: `none`,
                    }}
                >
                    Create Account
              </Link>
            </p>
            <button onClick={signin} className="btn btn-lg btn-primary btn-block" type="button" disabled={isDisabled()}>Login</button>
        </form>
    </Template>
}