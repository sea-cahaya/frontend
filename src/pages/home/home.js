import React, { useEffect, useState } from "react";
import { Template } from "../../layouts";
import './home.css';
import { getAllItems } from "../../usecase/apiService/home";
import { getLocalStorageUserData } from "../../usecase/utilities";
import { ItemCard } from "../../components/itemCard";

export default (props) => {
    const [items, setItems] = useState([]);

    const getItems = () => {
        getAllItems(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setItems(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        });
    }
    useEffect(getItems, []);
    

    return <Template>
        {items.length > 0 ? <div className="row row-cols-1 row-cols-sm-1 row-cols-md-3 g-4">
  
  {items.map((item, index)=>{
      return <div key={index} className="col"><ItemCard isPreview={false} data={item} /></div>
  })}
  </div> : <h1>no items yet</h1>}
    </Template>
};
