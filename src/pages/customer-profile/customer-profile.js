import React, { useState, useEffect } from "react";
import { getCustomerProfile, setCustomerProfile } from "../../usecase/apiService/customer";
import { getLocalStorageUserData } from "../../usecase/utilities";
import { Template } from "../../layouts";
import { AlertSuccess } from "../../components/alerts";


export default (props) => {
    const [profile, setProfile] = useState([]);
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');
    const [isLoading, setIsLoading] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const username = getLocalStorageUserData('username');

    useEffect(() => {
        getCustomerProfile(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setProfile(res.data);
                setName(res.data.name);
                setAddress(res.data.address);
                setPhone(res.data.phone);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }, [])

    const updateProfile = () => {
        setIsLoading(true);
        setCustomerProfile({ username, name, address, phone, authorization: getLocalStorageUserData('accessToken') }, {
            onSuccess: (res) => {
                setSuccessMessage(res.data.message);
                setShowSuccessMessage(true);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
        setIsLoading(false);
    }

    const isDisabled = () => {
        return !(name && address && phone && !isLoading)
    };

    return <Template>
        <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
        <form className="form-profile">
            <div className="text-center mb-4">
                <h1 className="h3 mb-3 font-weight-normal">Profile</h1>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setName(e.target.value);
                    }}
                    type="text"
                    id="inputName"
                    className="form-control"
                    placeholder="Name"
                    defaultValue={profile.name}
                    required
                />
                <label htmlFor="inputName">Name</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setAddress(e.target.value);
                    }}
                    type="text"
                    id="inputAddress"
                    className="form-control"
                    placeholder="Address"
                    defaultValue={profile.address}
                    required
                />
                <label htmlFor="inputAddress">Address</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setPhone(e.target.value);
                    }}
                    type="text"
                    id="inputPhone"
                    className="form-control"
                    placeholder="Phone"
                    defaultValue={profile.phone}
                    required
                />
                <label htmlFor="inputPhone">Phone</label>
            </div>
            <button onClick={updateProfile} disabled={isDisabled()} className="btn btn-lg btn-primary btn-block" type="button" >Update Profile</button>
        </form>
    </Template>
};
