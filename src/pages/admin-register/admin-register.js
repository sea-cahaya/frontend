import React, { useState } from "react";
import { Link } from 'react-router-dom';
import { TemplateAdmin } from "../../layouts";
import './admin-register.css';
import { AlertSuccess } from "../../components/alerts";
import { registerAdmin } from "../../usecase/apiService/auth"

export default () => {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [adminToken, setAdminToken] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');

    const register = () => {
        registerAdmin({ username, email, adminToken, password }, {
            onSuccess: (res) => {
                setSuccessMessage(res.data.message);
                setShowSuccessMessage(true);
                setTimeout(function () {
                    window.location = '/admin/login';
                }, 3000);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    };

    const isDisabled = () => {
        return !(username && email && adminToken && password.length >= 6 && confirmPassword && password === confirmPassword)
    }

    return <TemplateAdmin>
        <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
        <form className="form-signin">
            <div className="text-center mb-4">
                <h1 className="h3 mb-3 font-weight-normal">Register as Admin</h1>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setUsername(e.target.value);
                    }}
                    type="text"
                    id="inputUsername"
                    className="form-control"
                    placeholder="Username"
                    required
                />
                <label htmlFor="inputUsername">Username</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setEmail(e.target.value);
                    }}
                    type="email"
                    id="inputEmail"
                    className="form-control"
                    placeholder="Email address"
                    required
                />
                <label htmlFor="inputEmail">Email address</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setAdminToken(e.target.value);
                    }}
                    type="password"
                    id="inputToken"
                    className="form-control"
                    placeholder="Token"
                    required
                />
                <label htmlFor="inputToken">Token</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setPassword(e.target.value);
                    }}
                    type="password"
                    id="inputPassword"
                    className="form-control"
                    placeholder="Password (Min 6 Characters)"
                    required
                />
                <label htmlFor="inputPassword">Password (Min 6 Characters)</label>
            </div>
            <div className="form-label-group">
                <input
                    onChange={e => {
                        setConfirmPassword(e.target.value);
                    }}
                    type="password"
                    id="inputComfirmPassword"
                    className="form-control"
                    placeholder="Confirm Password"
                    required
                />
                <label htmlFor="inputComfirmPassword">Confirm Password</label>
            </div>
            <p>
                Have an account?{" "}
                <Link
                    to="/admin/login"
                    style={{
                        textDecoration: `none`,
                    }}
                >
                    Login
              </Link>
            </p>
            <button onClick={register} className="btn btn-lg btn-primary btn-block" type="button" disabled={isDisabled()}>Create Account</button>
        </form>
    </TemplateAdmin>
}