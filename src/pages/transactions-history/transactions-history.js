import React, { useState, useEffect } from 'react'
import { Template } from '../../layouts';
import './transactions-history.css';
import { getTransactionHistory } from '../../usecase/apiService/customer';
import { getLocalStorageUserData, convertToRupiah } from '../../usecase/utilities';

export default (props) => {
    const [transactions, setTransactions] = useState([]);

    useEffect(() => {
        getTransactionHistory(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setTransactions(res.data);
                console.log(res.data);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }, []);

    return <Template>
        <h2 className="text-center">Transactions History</h2>
        <TransactionsHistoryTable data={transactions} />
    </Template>
}

const TableRow = ({ data }) => data.map((transaction, index) => {
    return <tr key={transaction.id} className="np-table-row">
        <th className="fit" scope="row">{index + 1}</th>
        <td>{ new Date(transaction.transactionTime).toLocaleDateString() }</td>
        <td>{transaction.items.map((item, index) => (index ? ', ' : '') + item.itemName)}</td>
        <td>{convertToRupiah(transaction.totalPrice)}</td>
        <td style={{ color: transaction.status === "accepted" ? "#81a991" : "#d72638" }}>{transaction.status}</td>
    </tr>
})

const TransactionsHistoryTable = ({data}) => {
    return <table className="np-table np-0-8x">
        <thead>
            <tr className="np-table-heading">
                <th scope="col">No.</th>
                <th scope="col">Transaction Date</th>
                <th scope="col">Items</th>
                <th scope="col">Total Price</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
           <TableRow data={data} />
        </tbody>
    </table>
}