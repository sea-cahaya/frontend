import React, { useState, useEffect } from "react";
import { Template } from "../../layouts";
import './cart.css';
import { getLocalStorageUserData } from "../../usecase/utilities";
import { deleteCartItem, getCartItems } from "../../usecase/apiService/customer";
import { Link } from "react-router-dom";

export default (props) => {
    const [items, setItems] = useState([]);

    const getCart = () => {
        getCartItems(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setItems(res.data);
                console.log(res.data)
            },
            onError: (e) => {
                alert(e.message);
            }
        });
    }
    useEffect(getCart);
    return <Template>
        <h1>My Cart</h1>
        <CartTable data={items} />
        <Link to="/checkout" className="btn btn-outline-success" style={{marginTop: 30}}>Checkout</Link>
    </Template>
};

const TableRow = ({data}) => data.map((item, index) => {
    const deleteItem = () => {
        deleteCartItem(
            {
                cartId: item.cartId,
                authorization: getLocalStorageUserData("accessToken"),
            },
            {
                onSuccess: (res) => {
                    console.log(res.data);
                    alert(res.data.message);
                },
                onError: (e) => {
                    alert(e.message);
                    console.log(e.message);
                },
            }
        );
    }

    return <tr key={item.id} className="np-table-row">
        <th scope="row">{index + 1}</th>
        <td>{item.itemName}</td>
        <td>{item.price}</td>
        <td>{item.amount}</td>
        <td><button
                onClick={deleteItem}
                className={"btn btn-sm btn-outline-danger"}
                type="button">
            <svg
                width="1em"
                height="1em"
                viewBox="0 0 16 16"
                className="bi bi-trash"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg">
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                <path
                    fillRule="evenodd"
                    d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                />
            </svg>
        </button></td>
    </tr>
})

const CartTable = ({ data }) => {
    if (data.length > 0) return <table className="np-table np-0-8x">
        <thead>
            <tr className="np-table-heading">
                <th scope="col">No.</th>
                <th scope="col">Item Name</th>
                <th scope="col">Price</th>
                <th scope="col">Amount</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
           <TableRow data={data} />
        </tbody>
    </table>
    else return <h2>No items yet.</h2>
};

