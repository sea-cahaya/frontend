import React from "react";
import { Template } from "../../layouts";
import Cat from '../../images/404.svg';

export default () => {
    return <Template>
        <img src={Cat} alt="page not found"></img></Template>
}