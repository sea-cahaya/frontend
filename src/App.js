import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import routes from "./routes";
import './App.css';

function App() {
  return (
    <Router>
      <Suspense fallback={
        <div style={{ marginTop: '-40px', marginLeft: '-40px', position: 'absolute', top: '50%', left: '50%' }}>
          <h1>Loading</h1>
        </div>
      }>
        <Switch>
          {routes.map(route => {
            return <Route key={route.path} {...route} />
          })}
          <Redirect to="/404" />
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;
