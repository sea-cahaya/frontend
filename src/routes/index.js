import { Register, Fallback, Login, ApplyMerchant, Home, AdminHome, AdminLogin, AdminRegister, Profile, CustomerProfile, Store, Cart, Checkout, MerchantStore, AdminTransactions, MerchantRequestItem, TransactionsHistory, Wallet } from "../pages";

/**
 * berisi list objek konfigurasi untuk routes.
 */
const routes = [
    {
        path: "/",
        component: Home,
        exact: true,
    },
    {
        path: "/merchant/:id",
        component: MerchantStore,
        exact: true,
    },
    {
        path: "/register",
        component: Register,
        exact: true,
    },
    {
        path: "/profile",
        component: Profile,
        exact: true,
    },
    {
        path: "/request-item",
        component: MerchantRequestItem,
        exact: true,
    },
    {
        path: "/store",
        component: Store,
        exact: true,
    },
    {
        path: "/cart",
        component: Cart,
        exact: true,
    },
    {
        path: "/checkout",
        component: Checkout,
        exact: true,
    },
    {
        path: "/admin/register",
        component: AdminRegister,
        exact: true,
    },
    {
        path: "/wallet",
        component: Wallet,
        exact: true,
    },
    {
        path: "/login",
        component: Login,
        exact: true,
    },
    {
        path: "/admin/login",
        component: AdminLogin,
        exact: true,
    },
    {
        path: "/admin/home",
        component: AdminHome,
        exact: true,
    },
    {
        path: "/admin/merchant",
        component: ApplyMerchant,
        exact: true,
    },
    {
        path: "/admin/transactions",
        component: AdminTransactions,
        exact: true,
    },
    {
        path: "/404",
        component: Fallback,
        exact: true,
    },
    {
        path: "/customer/profile",
        component: CustomerProfile,
        exact: true,
    },
    {
        path: "/transactions-history",
        component: TransactionsHistory,
        exact: true,
    }
];

export default routes;
