import React from "react";

export const Input = (props) => {
    return <div className="form-label-group">
    <input
        {...props}
        placeholder={"#"}
        className="np-form-element np-text-foreground"
        required
    />
    <label htmlFor="inputStock">{props.label}</label>
</div>
};