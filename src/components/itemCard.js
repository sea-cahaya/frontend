import React, { useState, useEffect } from "react";
import {
    editItem,
    getItemById,
    deleteItemById,
} from "../usecase/apiService/merchant";
import { getLocalStorageUserData, convertToRupiah } from "../usecase/utilities";
import { addCartItem } from "../usecase/apiService/customer";
import { Link } from "react-router-dom";
import { Input } from "./input";

export const ItemCard = ({ data, isPreview }) => {
    const [isLoading, setIsLoading] = useState(false);
    const { id, merchantName, merchantId, name, description, price } = data;

    const addToCart = () => {
        if (!isPreview) {
            setIsLoading(true);
            addCartItem(
                {
                    itemId: id,
                    itemAmount: 1,
                    authorization: getLocalStorageUserData("accessToken"),
                },
                {
                    onSuccess: (res) => {
                        console.log(res.data);
                        alert(res.data.message);
                    },
                    onError: (e) => {
                        alert(e.message);
                        console.log(e.message);
                    },
                }
            );
            setIsLoading(false);
        }
    };
    const buyNow = () => {
        if (!isPreview) {
            addCartItem(
                {
                    itemId: id,
                    itemAmount: 1,
                    authorization: getLocalStorageUserData("accessToken"),
                },
                {
                    onSuccess: (res) => {
                        console.log(res.data);
                        setTimeout(function () {
                            window.location = "/checkout";
                        }, 3000);
                    },
                    onError: (e) => {
                        alert(e.message);
                        console.log(e.message);
                    },
                }
            );
        }
    };

    const isDisabled = () => {
        return !(!isLoading)
    };

    return (
        <div
            key={id}
            className="card np-element np-hover-inverse np-shadow-inverse"
            style={{ width: "100%" }}
        >
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <Link to={`/merchant/${merchantId}`} style={{ textDecoration: "none" }}>
                    <figcaption className="card-subtitle mb-2 blockquote-footer">
                        {merchantName || "Anonymous Merchant " + merchantId}
                    </figcaption>
                </Link>
                <h6 className="card-subtitle mb-2 text-success">
                    {convertToRupiah(price)}
                </h6>
                <p className="card-text">{description}</p>
                <hr></hr>
                <div className="row">
                    <div className="col">
                        <button
                            onClick={buyNow}
                            className="btn btn-sm btn-primary"
                            type="button"
                            disabled={isDisabled()}
                        >
                            Buy Now
            </button>
                    </div>
                    <div className="col d-flex justify-content-end">
                        <button
                            onClick={addToCart}
                            className="btn btn-sm btn-primary"
                            type="button"
                            disabled={isDisabled()}
                        >
                            <svg
                                width="1.3em"
                                height="1.3em"
                                viewBox="0 0 16 16"
                                className="bi bi-cart2"
                                fill="currentColor"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"
                                />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export const EditProfileItemCard = ({ data, onDeleteItem }) => {
    const { id, merchantId, name, description, price, stock } = data;
    const [itemName, setItemName] = useState(name)
    const [itemDescription, setItemDescription] = useState(description);
    const [itemPrice, setItemPrice] = useState(price);
    const [itemStock, setItemStock] = useState(stock);
    const [reload, setReload] = useState(false);
    const [displayMerchantId, setDisplayMerchantId] = useState(merchantId);
    const [displayName, setDisplayName] = useState(name);
    const [displayDescription, setDisplayDescription] = useState(description);
    const [displayPrice, setDisplayPrice] = useState(price);
    const [displayStock, setDisplayStock] = useState(stock);
    const [isLoading, setIsLoading] = useState(false);

    const editMerchantItem = () => {
        const body = {
            description: itemDescription,
            name: itemName,
            price: itemPrice,
            stock: itemStock,
        };

        if (window.confirm(`Are you sure you want to edit this item?`)) {
            setIsLoading(true);
            editItem(
                { id: id, body, authorization: getLocalStorageUserData("accessToken") },
                {
                    onSuccess: (res) => {
                        console.log(res.data);
                        getDetail(id);
                    },
                    onError: (e) => {
                        alert(e.message);
                        console.log(e.message);
                    },
                }
            );
            setIsLoading(false);
        }
    };

    const deleteMerchantItem = () => {
        if (window.confirm(`Are you sure you want to delete ${itemName}?`)) {
            setIsLoading(true);
            deleteItemById(
                { id, authorization: getLocalStorageUserData("accessToken") },
                {
                    onSuccess: () => {
                        onDeleteItem();
                    },
                    onError: (e) => {
                        alert(e.message);
                        console.log(e.message);
                    },
                }
            );
            setIsLoading(false);
        }
    };

    const getDetail = (id) => {
        getItemById(
            { id, authorization: getLocalStorageUserData("accessToken") },
            {
                onSuccess: (res) => {
                    console.log(res)
                    let data = res.data[0];
                    setDisplayName(data.name);
                    setDisplayMerchantId(data.merchantId);
                    setDisplayDescription(data.description);
                    setDisplayPrice(data.price);
                    setDisplayStock(data.stock);
                    setReload(false);
                },
                onError: (e) => {
                    alert(e.message);
                    console.log(e.message);
                },
            }
        );
    }

    useEffect(() => {
        if (reload) {
            getDetail(id)
        }
        // eslint-disable-next-line
    }, [reload]);

    const isDisabled = () => {
        return !(
            !isLoading &&
            itemName &&
            itemDescription &&
            itemPrice &&
            itemStock &&
            (itemName !== displayName ||
                itemDescription !== displayDescription ||
                itemPrice !== displayPrice ||
                itemStock !== displayStock)
        );
    };

    return (
        <div key={id} className="container">
            <div className="row g-2 align-items-center">
                <div className="col d-flex justify-content-center">
                    <div
                        className="card m-3 np-element np-shadow-inverse"
                        style={{ width: "18rem" }}
                    >
                        <div className="card-body">
                            <div className="form-label-group">
                                <input
                                    onChange={(e) => {
                                        setItemName(e.target.value);
                                    }}
                                    type="text"
                                    value={itemName}
                                    id="inputName"
                                    className="np-form-element np-text-foreground"
                                    placeholder="Item Name"
                                    required
                                />
                                <label htmlFor="inputName">Item Name</label>
                            </div>
                            <div className="form-label-group">
                                <input
                                    onChange={(e) => {
                                        setItemDescription(e.target.value);
                                    }}
                                    type="text"
                                    value={itemDescription}
                                    id="inputDescription"
                                    className="np-form-element np-text-foreground"
                                    placeholder="Item Name"
                                    required
                                />
                                <label htmlFor="inputDescription">Item Description</label>
                            </div>
                            <div className="form-label-group">
                                <input
                                    onChange={(e) => {
                                        setItemPrice(e.target.value);
                                    }}
                                    type="text"
                                    value={itemPrice}
                                    id="inputPrice"
                                    className="np-form-element np-text-foreground"
                                    placeholder="Item Name"
                                    required
                                />
                                <label htmlFor="inputPrice">Item Price</label>
                            </div>
                            <div className="form-label-group">
                                <input
                                    onChange={(e) => {
                                        setItemStock(e.target.value);
                                    }}
                                    type="text"
                                    value={itemStock}
                                    id="inputStock"
                                    className="np-form-element np-text-foreground"
                                    placeholder="Item Name"
                                    required
                                />
                                <label htmlFor="inputStock">Item Stock</label>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <button
                                        onClick={deleteMerchantItem}
                                        className={"btn btn-sm btn-outline-danger"}
                                        type="button"
                                    >
                                        <svg
                                            width="1em"
                                            height="1em"
                                            viewBox="0 0 16 16"
                                            className="bi bi-trash"
                                            fill="currentColor"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                            <path
                                                fillRule="evenodd"
                                                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                                            />
                                        </svg>
                                    </button>
                                </div>
                                <div className="col d-flex justify-content-end">
                                    <button
                                        onClick={editMerchantItem}
                                        className={
                                            isDisabled()
                                                ? "btn btn-sm btn-secondary"
                                                : "btn btn-sm btn-primary"
                                        }
                                        type="button"
                                        disabled={isDisabled()}
                                    >
                                        Edit
                  </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col d-flex justify-content-center">
                    <ItemCard
                        data={{
                            id,
                            merchantId: displayMerchantId,
                            name: displayName,
                            description: displayDescription,
                            price: displayPrice,
                            stock: displayStock,
                        }}
                    />
                </div>
            </div>
        </div>
    );
};

export const AddProfileItemCard = ({ onAddItem }) => {
    const [itemName, setItemName] = useState("");
    const [itemDescription, setItemDescription] = useState("");
    const [itemPrice, setItemPrice] = useState("");
    const [itemStock, setItemStock] = useState("");

    const isDisabled = () => {
        return !(itemName && itemDescription && itemPrice && itemStock);
    };

    const resetForm = () => {
        setItemName("");
        setItemDescription("");
        setItemPrice("");
        setItemStock("");
    };
    return (
        <div className="container">
            <div className="row g-2 align-items-center">
                <div className="col d-flex justify-content-center">
                    <div
                        className="card m-3 np-element np-shadow-inverse"
                        style={{ width: "18rem" }}
                    >
                        <div className="card-body">
                            <Input
                                onChange={(e) => {
                                    setItemName(e.target.value);
                                }}
                                type={"text"}
                                value={itemName}
                                id={"inputName"}
                                label={"Name"}
                                required
                            />
                            <Input
                                onChange={(e) => {
                                    setItemDescription(e.target.value);
                                }}
                                type={"text"}
                                value={itemDescription}
                                id={"inputDescription"}
                                className={"np-form-element np-text-foreground"}
                                label={"Description"}
                                required
                            />
                            <Input
                                onChange={(e) => {
                                    setItemPrice(e.target.value);
                                }}
                                type={"text"}
                                value={itemPrice}
                                id={"inputPrice"}
                                className={"np-form-element np-text-foreground"}
                                label={"Price"}
                                required
                            />
                            <Input
                                onChange={(e) => {
                                    setItemStock(e.target.value);
                                }}
                                type={"text"}
                                value={itemStock}
                                id={"inputStock"}
                                className={"np-form-element np-text-foreground"}
                                label={"Stock"}
                                required
                            />
                            <button
                                onClick={() => {
                                    onAddItem({
                                        name: itemName,
                                        description: itemDescription,
                                        price: itemPrice,
                                        stock: itemStock,
                                    });
                                    resetForm();
                                }}
                                className={
                                    isDisabled()
                                        ? "btn btn-sm btn-secondary btn-block"
                                        : "btn btn-sm btn-primary btn-block"
                                }
                                type="button"
                                disabled={isDisabled()}
                            >
                                Add
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col d-flex justify-content-center">
                    <ItemCard
                        data={{
                            merchantId: getLocalStorageUserData("id"),
                            name: itemName,
                            description: itemDescription,
                            price: itemPrice,
                            stock: itemStock,
                        }}
                        isPreview={true}
                    />
                </div>
            </div>
        </div>
    );
};
