import React, { Fragment, useState } from "react";

const TableRow = ({ data }) =>
    data.map((item, index) => {
        return <TableRowItem key={index} item={item} index={index} />;
    });

const TableRowItem = ({ item, index }) => {
    const [isOpen, setIsOpen] = useState(false);
    const SVG = () => {
        console.log(document.getElementById("tr-merchant-" + index));
        if (!isOpen) {
            return (
                <svg
                    width="1em"
                    height="1em"
                    viewBox="0 0 16 16"
                    className="bi bi-arrows-expand"
                    fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        fillRule="evenodd"
                        d="M1 8a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 8zM7.646.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 1.707V5.5a.5.5 0 0 1-1 0V1.707L6.354 2.854a.5.5 0 1 1-.708-.708l2-2zM8 10a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L7.5 14.293V10.5A.5.5 0 0 1 8 10z"
                    />
                </svg>
            );
        } else {
            return (
                <svg
                    width="1em"
                    height="1em"
                    viewBox="0 0 16 16"
                    className="bi bi-arrows-collapse"
                    fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        fillRule="evenodd"
                        d="M1 8a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 8zm7-8a.5.5 0 0 1 .5.5v3.793l1.146-1.147a.5.5 0 0 1 .708.708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 1 1 .708-.708L7.5 4.293V.5A.5.5 0 0 1 8 0zm-.5 11.707l-1.146 1.147a.5.5 0 0 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 11.707V15.5a.5.5 0 0 1-1 0v-3.793z"
                    />
                </svg>
            );
        }
    };
    return (
        <Fragment>
            <tr id={`tr-merchant-${index}`} key={index} className="np-table-row">
                <th scope="row">{index + 1}</th>
                <td>{item.customerName}</td>
                <td>{item.customerAddress}</td>
                <td>{item.customerPhone}</td>
                <td>{item.totalPrice}</td>
                <td>
                    <button
                        onClick={() => setIsOpen(!isOpen)}
                        data-toggle="collapse"
                        data-target={"#collapse" + index}
                        aria-expanded="false"
                        aria-controls={"collapse" + index}
                        type="button"
                        className="np-btn"
                    >
                        <SVG />
                    </button>
                </td>
            </tr>
            <tr
                id={"collapse" + index}
                className="collapse"
                aria-labelledby={"heading" + index}
                key={item.id}
            >
                <th scope="row" colSpan="6">
                    <table
                        className="np-table np-0-8x"
                        style={{ maxWidth: "60%", margin: "0 auto" }}
                    >
                        <thead>
                            <tr className="np-table-heading">
                                <th scope="col">No.</th>
                                <th scope="col">Item Name</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {item.responses.map((item, index) => {
                                return (
                                    <tr key={index} className="np-table-row">
                                        <th scope="row">{index + 1}</th>
                                        <td>{item.itemName}</td>
                                        <td>{item.amount}</td>
                                        <td>{item.price}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </th>
            </tr>
        </Fragment>
    );
};

export const MerchantRequestItemTable = ({ data }) => {
    if (data.length > 0)
        return (
            <div className="accordion" id="accordionExample">
                <table className="np-table np-0-8x">
                    <thead>
                        <tr className="np-table-heading">
                            <th scope="col">No.</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Customer Address</th>
                            <th scope="col">Customer Phone</th>
                            <th scope="col">Total Price</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <TableRow data={data} />
                    </tbody>
                </table>
            </div>
        );
    else return <h2>No new requests</h2>;
};
