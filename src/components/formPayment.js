import React, { useState } from 'react';
import { getLocalStorageUserData } from "../usecase/utilities";
import { buyFromCart } from '../usecase/apiService/customer';
import { AlertSuccess } from './alerts';

export const FormPayment = () => {
    const [bankName, setAccountName] = useState('');
    const [bankAccountNumber, setBankAccountNumber] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    
    const addTransaction = () => {
        buyFromCart({ bankName, bankAccountNumber, authorization: getLocalStorageUserData('accessToken') }, {
            onSuccess: (res) => {
                setSuccessMessage(res.data.message);
                setShowSuccessMessage(true);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }

    const isDisabled = () => {
        return !(bankName && bankAccountNumber)
    }

    return <form className="form-profile mt-3">
        <h2 className="h5 mb-3">Payment Details</h2>
        <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
        <div className="form-label-group">
            <input
                onChange={e => {
                    setAccountName(e.target.value);
                }}
                type="text"
                id="inputAccountName"
                className="form-control"
                placeholder="Account Name"
                required
            />
            <label htmlFor="inputAccountName">Your Bank Account Name</label>
        </div>
        <div className="form-label-group">
            <input
                onChange={e => {
                    setBankAccountNumber(e.target.value);
                }}
                type="text"
                id="inputBankAccountNumber"
                className="form-control"
                placeholder="Account Number"
                required
            />
            <label htmlFor="inputBankAccountNumber">Your Bank Account Number</label>
        </div>
        <button onClick={ addTransaction } disabled={ isDisabled() } className="btn btn-primary mb-3" type="button" >Submit Payment</button>
    </form>
}