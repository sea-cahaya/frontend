import React from "react";
export const AlertDanger = ({ text, isShown }) => {
    if (isShown) {
        return <div className="alert alert-danger alert-dismissible fade show" role="alert">
            {text}
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    } else {
        return ''
    }
};

export const AlertSuccess = ({ text, isShown }) => {
    if (isShown) {
        return <div className="alert alert-success alert-dismissible fade show" role="alert">
            {text}
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    } else {
        return ''
    }
};