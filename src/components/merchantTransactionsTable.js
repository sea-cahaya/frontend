import React from "react";
import { acceptTransaction, rejectTransaction } from "../usecase/apiService/admin";
import { getLocalStorageUserData } from "../usecase/utilities";

const TableRow = ({data}) => data.map((item, index) => {
    const accept = () => {
        acceptTransaction( {transactionId: item.id, authorization: getLocalStorageUserData('accessToken')}, {
            onSuccess: (res) => {
                let message = res.data.message || "Success!";
                alert(message);
                window.location.reload();
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    };
    const reject = () => {
        rejectTransaction( {transactionId: item.id, authorization: getLocalStorageUserData('accessToken')}, {
            onSuccess: (res) => {
                let message = res.data.message || "Success!";
                alert(message);
                window.location.reload();
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    };
    const convertDate = (rawDate) => {
        let date = new Date(rawDate);
        let day = date.getDate();
        let month = date.toLocaleString('default', { month: 'short' });
        // let month = date.getMonth() + 1;
        let year = date.getFullYear();
        let hour = date.getHours();
        let minute = date.getMinutes();
        return `${day} ${month} ${year}, ${hour}:${minute}`
    }
    return <tr key={item.id}>
        <th scope="row">{index + 1}</th>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.totalPrice}</td>
        <td>{item.bankName}</td>
        <td>{item.bankAccountNumber}</td>
        <td>{convertDate(item.transactionTime)}</td>
        <td>
            <button onClick={accept} type="button" className="btn btn-outline-success">Accept</button>
            <button onClick={reject} type="button" className="btn btn-outline-danger">Reject</button>
        </td>
    </tr>
})

export const MerchantTransactionTable = ({ data }) => {
    if (data.length > 0) return <table className="table">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Transaction ID</th>
                <th scope="col">Customer Name</th>
                <th scope="col">Total Price</th>
                <th scope="col">Bank Name</th>
                <th scope="col">Bank Account Number</th>
                <th scope="col">Time</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
           <TableRow data={data} />
        </tbody>
    </table>
    else return <h2>No new transactions</h2>
};
