import React from "react";
import { Link } from "react-router-dom";

export const NavLink = (props) => {
    return <Link
          style={{
            textDecoration: `none`,
            padding: '4px 16px'
          }}
          className="nav-link np-btn"
          {...props}
        >
          {props.name}
        </Link>
};