import React, { useState, useEffect } from "react";
import { getMerchantProfile, setMerchantProfile } from "../usecase/apiService/merchant";
import { getLocalStorageUserData } from "../usecase/utilities";
import { AlertSuccess } from "./alerts";


export const FormMerchantProfile = () => {

    const [profile, setProfile] = useState([]);
    const [username, setUsername] = useState('');
    const [address, setAddress] = useState('');
    const [merchantName, setMerchantName] = useState('');
    const [phone, setPhone] = useState('');
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        getMerchantProfile(getLocalStorageUserData('accessToken'), {
            onSuccess: (res) => {
                setProfile(res.data);
                setUsername(res.data.username);
                setMerchantName(res.data.merchantName);
                setAddress(res.data.address);
                setPhone(res.data.phone);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }, [])

    const updateProfile = () => {
        setIsLoading(true);
        setMerchantProfile({ username, address, merchantName, phone, authorization: getLocalStorageUserData('accessToken') }, {
            onSuccess: (res) => {
                setSuccessMessage(res.data.message);
                setShowSuccessMessage(true);
            },
            onError: (e) => {
                alert(e.message);
            }
        })
        setIsLoading(false);
    }

    const isDisabled = () => {
        return !(username && merchantName && address && phone && !isLoading)
    };

    return <form className="form-profile">
    <AlertSuccess text={successMessage} isShown={showSuccessMessage} />
    <div className="text-center mb-4">
        <h1 className="h3 mb-3 font-weight-normal">Merchant Profile</h1>
    </div>
    <div className="form-label-group">
        <input
            onChange={e => {
                setUsername(e.target.value);
            }}
            type="text"
            id="inputUsername"
            className="form-control"
            placeholder="Username"
            defaultValue={profile.username}
            required
        />
        <label htmlFor="inputUsername">Username</label>
    </div>
    <div className="form-label-group">
        <input
            onChange={e => {
                setAddress(e.target.value);
            }}
            type="text"
            id="inputAddress"
            className="form-control"
            placeholder="Address"
            defaultValue={profile.address}
            required
        />
        <label htmlFor="inputAddress">Address</label>
    </div>
    <div className="form-label-group">
        <input
            onChange={e => {
                setMerchantName(e.target.value);
            }}
            type="text"
            id="inputAddress"
            className="form-control"
            placeholder="Address"
            defaultValue={profile.merchantName}
            required
        />
        <label htmlFor="inputAddress">Merchant name</label>
    </div>
    <div className="form-label-group">
        <input
            onChange={e => {
                setPhone(e.target.value);
            }}
            type="text"
            id="inputPhone"
            className="form-control"
            placeholder="Phone"
            defaultValue={profile.phone}
            required
        />
        <label htmlFor="inputPhone">Phone</label>
    </div>
    <button onClick={updateProfile} disabled={isDisabled()} className="btn btn-lg btn-primary btn-block" type="button" >Update Profile</button>
</form>
};
