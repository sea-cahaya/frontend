import React from "react";
import { confirmMerchantApplication } from "../usecase/apiService/admin";
import { getLocalStorageUserData } from "../usecase/utilities";

const TableRow = ({data}) => data.map((item, index) => {
    const confirm = () => {
        confirmMerchantApplication( {username: item.username, authorization: getLocalStorageUserData('accessToken')}, {
            onSuccess: (res) => {
                let message = res.data.message || "Success!";
                alert(message);
                window.location.reload();
            },
            onError: (e) => {
                alert(e.message);
            }
        })
    }
    return <tr key={item.id}>
        <th scope="row">{index + 1}</th>
        <td>{item.username}</td>
        <td>{item.email}</td>
        <td><button onClick={confirm} type="button" className="btn btn-outline-success">Accept</button></td>
    </tr>
})

export const MerchantApplicationTable = ({ data }) => {
    if (data.length > 0) return <table className="table">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
           <TableRow data={data} />
        </tbody>
    </table>
    else return <h2>No new applications</h2>
};
