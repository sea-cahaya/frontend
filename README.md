This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Installation](#installation)
  - [node](#node)
  - [docker](#docker)
- [Folder Structure](#folder-structure)
  - [`components`](#components)
  - [`images`](#images)
  - [`layouts`](#layouts)
  - [`pages`](#pages)
  - [`usecase`](#usecase)
- [NPM Scripts](#npm-scripts)
  - [`npm dev`](#npm-dev)
  - [`npm start`](#npm-start)
  - [`npm test`](#npm-test)


## Installation

You need node version x to run this project. You can also directly run on Development or Production environment by using docker.

### node

```
# install project's dependecies
npm install

# run on development environment
# listening on http://localhost:3000
npm run dev

# run on production environment
# listening on http://localhost:5000
npm run prod

```

### docker

```
# run on development environment
# listening on http://localhost:3000
docker-compose -f docker-compose.dev.yml up

# run on production environment
# listening on http://localhost
docker-compose up
```

## Folder Structure

This project's structure is based on
https://reactjs.org/docs/faq-structure.html#grouping-by-features-or-routes.
We have 5 main directories inside `src`:

### `components`
Consist of UI components. UI components should be extracted to Components if that UI component is frequently used.

### `images`
Consist of image assets used in this project.

### `layouts`
Consist of page layouts. Used as a 'wrapper' to pages.

### `pages`
Consist of pages.

### `usecase`
Consist of function logics.

## NPM Scripts

In the project directory, you can run:

### `npm dev`

Runs the app in the DEVELOPMENT mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm start`

Runs the app in the PRODUCTION mode.<br />
Open [http://localhost:3000](http://localhost:5000) to view it in the browser.

The page will NOT reload if you make edits.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

To see the complete list, see [package.json](./package.json).